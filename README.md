## Geolocator

### Ruby on Rails API

Run `rails db:seed`
Add `IPSTACK_ACCESS_KEY` env

Find geolocation:
```
GET /geolocation?ip=example_ip
```

Create geolocation:
```
POST /geolocation?ip=example_ip
```

Delete geolocation:
```
DELETE /geolocation?ip=example_ip
```
