require 'rails_helper'

RSpec.describe GetGeolocationDataJob, type: :job do
  describe '#perform_later' do
    it 'gets geolocation data' do
      ActiveJob::Base.queue_adapter = :test
      expect {
        described_class.perform_later(1)
      }.to have_enqueued_job
    end
  end
end
