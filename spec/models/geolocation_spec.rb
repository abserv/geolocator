require 'rails_helper'

RSpec.describe Geolocation, type: :model do
  describe 'CreateForm' do
    context 'invalid data' do
      context 'record does not exist' do
        describe 'invalid web_address' do
          let(:web_address) { 'foobar' }
          let(:form) do
            Geolocation::CreateForm.new.call(web_address: web_address)
          end

          it 'returns errors' do
            expect(form.errors.to_h[:web_address]).to include 'invalid address'
          end
        end

        describe 'invalid ip' do
          let(:web_address) { 'http://www.wp.pl,123.123.123.123' }

          it 'returns errors' do
            form = Geolocation::CreateForm.new.call(web_address: web_address)
            expect(form.errors.to_h[:web_address]).to include 'bulk action not implemented'
            expect(form.errors.to_h[:web_address]).to include 'invalid address'
          end
        end
      end

      context 'record exists' do
        let(:web_address) { '123.123.123.123' }
        let(:form) do
          Geolocation::CreateForm.new
            .call(web_address: web_address, ip: web_address)
        end

        before do
          create(:geolocation, ip: web_address)
        end

        it 'returns errors' do
          expect(form.errors.to_h[:web_address]).to include 'already exists'
        end
      end
    end

    context 'valid data' do
      let(:web_address) { '123.123.123.123' }
      let(:form) do
        Geolocation::CreateForm.new
        .call(web_address: web_address, ip: web_address)
      end

      it 'does not return errors' do
        expect(form.errors.empty?).to be true
      end
    end
  end
end
