require 'rails_helper'

RSpec.describe 'GeolocationsController', type: :request do
  let(:valid_ip) { '134.201.250.155' }

  context 'show' do
    context 'geolocation exists' do
      describe 'GET /geolocation?ip=134.201.250.155' do
        let(:ip) { valid_ip }
        let!(:geolocation) { create(:geolocation, {web_address: ip}) }

        it 'renders :show template' do
          get "/geolocation?ip=#{ip}"
          expect(response).to render_template(:show)
          expect(response).to have_http_status(:ok)
        end
      end
    end

    context 'geolocation does not exist' do
      describe 'GET /geolocation?ip=11' do
        let(:ip) { valid_ip }
        let(:body) { {geolocation: 'not found'}.to_json }

        it 'renders json response' do
          get "/geolocation?ip=#{ip}"
          expect(response.body).to eq body
        end
      end
    end
  end

  context 'create' do
    context 'valid data' do
      describe 'POST /geolocation?ip=134.201.250.155' do
        let(:ip) { valid_ip }
        it 'renders json response' do
          expect(GetGeolocationDataJob).to receive(:perform_later)

          expect { post '/geolocation', params: {ip: ip} }
            .to change(Geolocation, :count).by(1)
          expect(response).to have_http_status(:ok)
        end
      end
    end

    context 'invalid data' do
      describe 'POST /geolocation?ip=foo' do
        let(:ip) { 'foo' }

        it 'renders json response' do
          expect { post '/geolocation', params: {ip: ip} }
            .not_to change(Geolocation, :count)
          expect(response).to have_http_status(422)
        end
      end
    end
  end

  context 'destroy' do
    context 'geolocation exists' do
      let(:ip) { valid_ip }
      let!(:geolocation) { create(:geolocation, {web_address: ip}) }

      describe 'DELETE /geolocation?ip=134.201.250.155' do
        it 'renders json response' do
          expect { delete '/geolocation', params: {ip: ip} }
          .to change(Geolocation, :count).by(-1)
          expect(response).to have_http_status(:ok)
        end
      end
    end


    context 'geolocation does not exist' do
      describe 'DELETE /geolocation?ip=11' do
        let(:ip) { valid_ip }
        let(:body) { {geolocation: 'not found'}.to_json }

        it 'renders json response' do
          delete '/geolocation', params: {ip: ip}
          expect(response.body).to eq body
        end
      end
    end
  end
end
