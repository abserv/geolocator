FactoryBot.define do
  factory :geolocation do
    web_address { "MyString" }
    ip { "MyString" }
    status { "MyString" }
    data_source { "MyString" }
    geo_data { "" }
  end
end
