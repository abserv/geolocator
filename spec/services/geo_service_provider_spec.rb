require 'rails_helper'

RSpec.describe GeoServiceProvider do

  describe 'Ipstack' do
    let(:geolocation) { create(:geolocation) }
    let(:provider) { GeoServiceProvider::Ipstack.new(geolocation.ip) }
    let(:service) { GeoServiceProvider.new(provider) }
    let(:body) do
      {ip: '134.201.250.155'}.to_json
    end

    before do
      stub_request(:get, /api.ipstack.com/).
        with(
          headers: {
           'Accept'=>'*/*',
           'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
           'Host'=>'api.ipstack.com',
           'User-Agent'=>'Ruby'
          }
        ).to_return(status: 200, body: body, headers: {})
    end

    it 'gets geo data' do
      service.run
      expect(service.status).to eq :valid
      expect(service.geo_data).to eq JSON.parse(body)
      expect(service.data_source).to eq 'ipstack'
    end
  end
end
