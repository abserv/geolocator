require 'rails_helper'

RSpec.describe WebAddressSanitizer do
  describe 'with invalid web address' do
    let(:web_address) { 'foobar' }
    let(:service) { described_class.new(web_address) }

    it 'returns nil' do
      expect(IPAddress).to receive(:valid?).with(web_address).and_return(false)
      expect(IPSocket).to receive(:getaddress).with(nil).and_return(nil)
      expect(service.ip).to be_nil
    end
  end

  describe 'with valid web address' do
    context 'type ip' do
      let(:web_address) { '212.77.98.9' }
      let(:service) { described_class.new(web_address) }

      it 'returns id' do
        expect(IPAddress).to receive(:valid?).with(web_address).and_return(true)
        expect(service.ip).to eq web_address
      end
    end

    context 'type url' do
      let(:web_address) { 'https://www.wp.pl' }
      let(:service) { described_class.new(web_address) }
      let(:ip) { '212.77.98.9' }

      it 'returns id' do
        expect(IPAddress).to receive(:valid?).with(web_address).and_return(false)
        expect(IPSocket).to receive(:getaddress).with('www.wp.pl').and_return(ip)
        expect(service.ip).to eq(ip)
      end
    end
  end
end
