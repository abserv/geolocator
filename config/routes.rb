Rails.application.routes.draw do
  scope '/', defaults: { format: :json } do
    resource :geolocation, only: [:create, :show, :destroy]
  end
end
