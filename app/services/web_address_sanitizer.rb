class WebAddressSanitizer
  attr_reader :web_address

  def initialize(web_address)
    @web_address = web_address
  end

  def ip
    return web_address if IPAddress.valid?(web_address)
    ip_from_url(get_host)
  end

  private

  def get_host
    begin
      URI(web_address).host
    rescue
    end
  end

  def ip_from_url(address)
    begin
      IPSocket.getaddress(address)
    rescue
    end
  end
end
