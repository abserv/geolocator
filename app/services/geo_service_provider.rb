class GeoServiceProvider
  attr_reader :provider

  def initialize(provider)
    @provider = provider
  end

  def run
    @geo_data = provider.get_geo_data
  end

  def geo_data
    @geo_data ||= run
  end

  def status
    if provider.success?
      :valid
    else
      :invalid
    end
  end

  def data_source
    provider.to_s
  end
end
