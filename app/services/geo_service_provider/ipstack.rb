require "net/http"

class GeoServiceProvider::Ipstack
  attr_reader :ip

  IpstackError = Class.new(StandardError)

  BASE_URL = "http://api.ipstack.com/"
  PARAMS = {
    access_key: ENV['IPSTACK_ACCESS_KEY'],
    format: 1,
  }

  def initialize(ip)
    @ip = ip
    @success = true
  end

  def geo_data
    @geo_data ||= get_geo_data
  end

  def get_geo_data
    uri = URI::join(BASE_URL, ip)
    uri.query = URI.encode_www_form(PARAMS)
    response = Net::HTTP.get_response(uri)

    if response.code != '200'
      raise IpstackError, "response code: #{response.code}"
    end

    @success = false if response.body['error']

    @geo_data = JSON.parse(response.body)
  end

  def to_s
    'ipstack'
  end

  def success?
    @success
  end
end
