class GetGeolocationDataJob < ApplicationJob
  queue_as :default
  retry_on Exception, wait: :exponentially_longer

  def perform(geolocation_id)
    geolocation = Geolocation.find(geolocation_id)

    return if geolocation.nil?
    return unless geolocation.status == 'pending'

    provider = GeoServiceProvider::Ipstack.new(geolocation.ip)
    service = GeoServiceProvider.new(provider)

    service.run

    geolocation.update!({
      status: service.status,
      geo_data: service.geo_data,
      data_source: service.data_source,
    })
  end
end
