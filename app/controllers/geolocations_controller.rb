class GeolocationsController < ApplicationController
  def create
    ip = WebAddressSanitizer.new(params[:ip]).ip
    form = Geolocation::CreateForm.new.call(web_address: params[:ip], ip: ip)
    geolocation =
      Geolocation.new(form.to_h.merge({
        status: :pending,
        data_source: :ipstack,
      }))

    if form.errors.empty? && geolocation.save
      GetGeolocationDataJob.perform_later(geolocation.id)
      render json: :ok
    else
      render json: { errors: form.errors.to_h }, status: 422
    end
  end

  def show
    @geolocation = Geolocation.where(web_address: params[:ip]).first

    if @geolocation
      render
    else
      render json: { geolocation: 'not found' }
    end
  end

  def destroy
    geolocation = Geolocation.where(web_address: params[:ip]).first
    if geolocation
      geolocation.destroy
      render json: :ok
    else
      render json: { geolocation: 'not found' }
    end
  end
end
