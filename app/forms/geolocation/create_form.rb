class Geolocation::CreateForm < Dry::Validation::Contract
  params do
    required(:web_address).filled(:string)
    optional(:ip).value(:string)
  end

  rule(:web_address) do
    if values[:web_address].split(',').count > 1
      key.failure('bulk action not implemented')
    end
  end

  rule(:web_address, :ip) do
    if values[:ip].present?
      unless Geolocation.where("LOWER(ip) = ?", values[:ip].downcase).empty?
        key.failure('already exists')
      end
    else
      key.failure('invalid address')
    end
  end
end
