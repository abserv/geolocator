info_1 = {
  "ip": "134.201.250.155",
  "hostname": "134.201.250.155",
  "type": "ipv4",
  "continent_code": "NA",
  "continent_name": "North America",
  "country_code": "US",
  "country_name": "United States",
  "region_code": "CA",
  "region_name": "California",
  "city": "Los Angeles",
  "zip": "90013",
  "latitude": 34.0453,
  "longitude": -118.2413,
  "location": {
    "geoname_id": 5368361,
    "capital": "Washington D.C.",
    "languages": [
        {
          "code": "en",
          "name": "English",
          "native": "English"
        }
    ],
    "country_flag": "https://assets.ipstack.com/images/assets/flags_svg/us.svg",
    "country_flag_emoji": "🇺🇸",
    "country_flag_emoji_unicode": "U+1F1FA U+1F1F8",
    "calling_code": "1",
    "is_eu": false
  },
  "time_zone": {
    "id": "America/Los_Angeles",
    "current_time": "2018-03-29T07:35:08-07:00",
    "gmt_offset": -25200,
    "code": "PDT",
    "is_daylight_saving": true
  },
  "currency": {
    "code": "USD",
    "name": "US Dollar",
    "plural": "US dollars",
    "symbol": "$",
    "symbol_native": "$"
  },
  "connection": {
    "asn": 25876,
    "isp": "Los Angeles Department of Water & Power"
  },
  "security": {
    "is_proxy": false,
    "proxy_type": nil,
    "is_crawler": false,
    "crawler_name": nil,
    "crawler_type": nil,
    "is_tor": false,
    "threat_level": "low",
    "threat_types": nil
  }
}

info_error = {
  "success": false,
  "error": {
    "code": 106,
    "type": "invalid_ip_address",
    "info": "The IP Address supplied is invalid."
  }
}

Geolocation.create({
  web_address: '134.201.250.155',
  ip: '134.201.250.155',
  status: :valid,
  data_source: :ipstack,
  info: info_1,
})

Geolocation.create({
  web_address: '134.201.250.667',
  ip: '134.201.250.667',
  status: :invalid,
  data_source: :ipstack,
  info: info_error,
})
