class CreateGeolocations < ActiveRecord::Migration[7.0]
  def change
    create_table :geolocations do |t|
      t.string :web_address
      t.string :ip
      t.string :status
      t.string :data_source
      t.jsonb :geo_data

      t.timestamps

      t.index :web_address
    end
  end
end
